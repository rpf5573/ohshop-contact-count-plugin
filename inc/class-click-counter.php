<?php

class ClickCounter
{
  public function __construct()
  {
    add_action('wp_enqueue_scripts', array($this, 'enqueue_click_counter_scripts'));
    add_action('admin_enqueue_scripts', array($this, 'enqueue_click_counter_admin_styles'));
    add_action('wp_ajax_click_counter', array($this, 'click_counter_callback'));
    add_action('wp_ajax_nopriv_click_counter', array($this, 'click_counter_callback'));
    add_action('admin_post_delete_old_clicks', array($this, 'delete_old_clicks'));
    add_action('admin_menu', array($this, 'click_counter_menu'));
  }

  public static function create_custom_table()
  {
    global $wpdb;

    $table_name = $wpdb->prefix . 'click_counter';
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id bigint(20) UNSIGNED AUTO_INCREMENT,
        ip_address varchar(45) NOT NULL,
        click_date date NOT NULL,
        contact_type varchar(10) NOT NULL,
        PRIMARY KEY (id)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql); // DB에서 테이블을 찾아서, 필요한 부분만 업데이트를 해줌. 테이블이 없으면 새로 생성함
  }

  public function enqueue_click_counter_admin_styles()
  {
    wp_enqueue_style('click-counter', OHSHOP_PLUGIN_URL . 'assets/css/statistics-table.css');
  }

  public function enqueue_click_counter_scripts()
  {
    wp_enqueue_script('jquery');
    wp_enqueue_script('click-counter', OHSHOP_PLUGIN_URL . 'assets/js/click-counter.js', array('jquery'), '1.0.0', true);

    // Get user's IP address
    $ip_address = $_SERVER['REMOTE_ADDR'];

    wp_localize_script(
      'click-counter',
      'ajax_object',
      array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'ip_address' => $ip_address
      )
    );
  }

  public function click_counter_callback()
  {
    global $wpdb;
    $table_name = $wpdb->prefix . 'click_counter';

    $ip_address = $_POST['ip_address'];
    $click_date = $_POST['click_date'];
    $click_type = $_POST['click_type'];

    // Check if IP address has already clicked today for the given type
    $result = $wpdb->get_row(
      $wpdb->prepare(
        "SELECT * FROM $table_name WHERE ip_address = %s AND click_date = %s AND contact_type = %s",
        $ip_address,
        $click_date,
        $click_type
      )
    );

    // If the IP address hasn't clicked today, insert the click data
    if (null === $result) {
      $wpdb->insert(
        $table_name,
        array(
          'ip_address' => $ip_address,
          'click_date' => $click_date,
          'contact_type' => $click_type
        ),
        array('%s', '%s', '%s')
      );
    }

    wp_die();
  }

  public function click_counter_menu()
  {
    add_menu_page(
      '카카오톡 상담 통계',
      '카카오톡 상담 통계',
      'manage_options',
      'click-counter',
      array(
        $this,
        'click_counter_admin_page'
      )
    );
  }

  public function click_counter_admin_page()
  {
    global $wpdb;
    $table_name = $wpdb->prefix . 'click_counter';
    $today = date('Y-m-d');
    $thirty_days_ago = date('Y-m-d', strtotime('-30 days'));

    // Start output buffering
    ob_start();
    ?>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <h1>카카오톡 상담 통계</h1>

    <?php
    $types = array('general', 'stock');
    foreach ($types as $type):
      // Fetch today's click count for the current type
      $todays_clicks = $wpdb->get_var(
        $wpdb->prepare(
          "SELECT COUNT(*) FROM $table_name WHERE click_date = %s AND contact_type = %s",
          $today,
          $type
        )
      );

      // Fetch the click counts for the last 30 days for the current type
      $thirty_day_clicks = $wpdb->get_results(
        $wpdb->prepare(
          "SELECT click_date, COUNT(*) as click_count FROM $table_name WHERE click_date BETWEEN %s AND %s AND contact_type = %s GROUP BY click_date ORDER BY click_date DESC",
          $thirty_days_ago,
          $today,
          $type
        )
      );

      // Generate an array with all the dates in the last 30 days
      $date_range = [];
      for ($i = 0; $i < 30; $i++) {
        $date_range[] = date('Y-m-d', strtotime("-$i days"));
      }

      function_exists('ray') && ray('data_range', $date_range);

      // Convert the thirty_day_clicks results into an associative array
      $click_data_by_date = [];
      foreach ($thirty_day_clicks as $click_data) {
        $click_data_by_date[$click_data->click_date] = $click_data->click_count;
      }

      // Fill in the missing dates with a click count of 0
      foreach ($date_range as $date) {
        if (!isset($click_data_by_date[$date])) {
          $click_data_by_date[$date] = 0;
        }
      }

      krsort($click_data_by_date);
      ?>

      <div class="click-counter-container">
        <h3>
          <?php
          if ($type === 'general') {
            echo '금일 카톡 상담하기 클릭 횟수: ';
          } else {
            echo '금일 카톡 재고확인 클릭 횟수: ';
          }
          ?>
          <?php echo $todays_clicks ?: 0; ?>
        </h3>

        <h3>최근 30일 기록</h3>
        <div class="click-counter-chart-container">
          <canvas id="<?php echo $type; ?>-chart" width="400" height="200"></canvas>
        </div>

        <script>
          document.addEventListener('DOMContentLoaded', function () {
            const data = <?php echo json_encode($click_data_by_date); ?>;
            const labels = Object.keys(data);
            const values = Object.values(data);

            const ctx = document.getElementById('<?php echo $type; ?>-chart').getContext('2d');
            const chart = new Chart(ctx, {
              type: 'bar',
              data: {
                labels: labels,
                datasets: [{
                  label: '상담횟수',
                  data: values,
                  backgroundColor: 'rgba(75, 192, 192, 0.2)',
                  borderColor: 'rgba(75, 192, 192, 1)',
                  borderWidth: 1
                }]
              },
              options: {
                scales: {
                  x: {
                    ticks: {
                      callback: function (value, index, values) {
                        return labels[index].split('-').slice(1).join('-');
                      }
                    }
                  },
                  y: {
                    beginAtZero: true
                  }
                }
              }
            });
          });
        </script>
      </div>
    <?php endforeach; ?>
    <form action="<?php echo admin_url('admin-post.php'); ?>" method="post">
      <input type="hidden" name="action" value="delete_old_clicks">
      <?php wp_nonce_field('delete_old_clicks'); ?>
      <button class="button button-primary" type="submit">30일 지난 데이터 삭제하기</button>
    </form>
    <?php
    echo ob_get_clean();
  }

  public function delete_old_clicks()
  {
    check_admin_referer('delete_old_clicks');

    global $wpdb;
    $table_name = $wpdb->prefix . 'click_counter';
    $thirty_days_ago = date('Y-m-d', strtotime('-31 days'));

    $wpdb->query(
      $wpdb->prepare(
        "DELETE FROM $table_name WHERE click_date < %s",
        $thirty_days_ago
      )
    );

    wp_redirect(admin_url('admin.php?page=click-counter'));
    exit;
  }
}
