(($) => {
  $(document).ready(function ($) {
    const handleClick = (selector, clickType) => {
      $(selector).on("click", function () {
        const ip_address = ajax_object.ip_address;
        const click_date = new Date().toISOString().slice(0, 10);

        $.post(ajax_object.ajax_url, {
          action: "click_counter",
          ip_address: ip_address,
          click_date: click_date,
          click_type: clickType,
        });
      });
    };

    handleClick("#plusfriend-chat-button", "general");
    handleClick(".elementor-element-a3b0f55 a", "general");
    handleClick(".elementor-element-24bea3f .elementor-button-link", "stock");
  });
})(jQuery);
