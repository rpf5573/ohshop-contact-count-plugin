<?php
/**
 * Plugin Name:     OhShop Kakao Contact Count Plugin
 * Plugin URI:      https://kmong.com/gig/155175
 * Description:     카카오톡으로 상담한 횟수 카운팅 플러그인입니다
 * Author:          YoonManager_WPMate
 * Author URI:      https://kmong.com/gig/155175
 * Text Domain:     ohshop
 * Version:         1.0
 *
 */

if (!defined('ABSPATH')) {
  exit;
}

if (!defined('OHSHOP_PLUGIN_DIR')) {
  define('OHSHOP_PLUGIN_DIR', plugin_dir_path(__FILE__));
}

if (!defined('OHSHOP_PLUGIN_URL')) {
  define('OHSHOP_PLUGIN_URL', plugin_dir_url(__FILE__));
}

require_once(OHSHOP_PLUGIN_DIR . 'inc/class-click-counter.php');

register_activation_hook(__FILE__, 'ClickCounter::create_custom_table');

function ohshop_initialize_click_counter()
{
  new ClickCounter();
}
add_action('plugins_loaded', 'ohshop_initialize_click_counter');
